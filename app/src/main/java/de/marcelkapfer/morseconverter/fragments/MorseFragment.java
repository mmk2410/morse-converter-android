package de.marcelkapfer.morseconverter.fragments;

/*
    This is a Android application for converting writtenMorse and normal morse code.
    Copyright (C) 2014-2015  Marcel Michael Kapfer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Marcel Michael Kapfer
    marcelmichaelkapfer@yahoo.co.nz

 */

import android.app.Activity;
import android.content.Context;
import android.inputmethodservice.KeyboardView;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.KeyListener;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import de.marcelkapfer.morseconverter.MainActivity;
import de.marcelkapfer.morseconverter.R;
import de.marcelkapfer.morseconverter.engine.DecodeNormalMorseManager;
import de.marcelkapfer.morseconverter.engine.EncodeNormalMorseManager;
import de.marcelkapfer.morseconverter.intelligentCodeRecognization.NormalMorseCodeRecognization;

/**
 * Created by mmk on 2/14/15.
 */
public class MorseFragment extends Fragment {
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_morse, container, false);
    }

    @Override
    public void onStart() {

        LinearLayout normalMorseCopy = (LinearLayout) getActivity().findViewById(R.id.copyNormalMorse);

        normalMorseCopy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                copyNormalMorse(getActivity());
            }
        });

        LinearLayout normalMorseShare = (LinearLayout) getActivity().findViewById(R.id.shareNormalMorse);

        normalMorseShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareNormalMorse(getActivity());
            }
        });

        EditText input = (EditText) getActivity().findViewById(R.id.editTextNormalMorse);

        final ConvertThread convertThread = new ConvertThread(getActivity());

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                convertThread.run();
            }
        });

        super.onStart();
    }

    class ConvertThread extends Thread {

        FragmentActivity fragmentActivity;

        ConvertThread(FragmentActivity fA) {
            fragmentActivity = fA;
        }

        public void run() {
            normalMorseConvert(fragmentActivity);
        }

        public void normalMorseConvert(FragmentActivity fragmentActivity) {
            try {
                EditText input = (EditText) fragmentActivity.findViewById(R.id.editTextNormalMorse);
                TextView output = (TextView) fragmentActivity.findViewById(R.id.outputNormalMorse);
                CardView cardView = (CardView) fragmentActivity.findViewById(R.id.cardViewNormalMorseOutput);
                if (cardView.getVisibility() == View.INVISIBLE) {
                    cardView.setVisibility(View.VISIBLE);
                }
                String inputString = input.getText().toString();
                if (NormalMorseCodeRecognization.isCode(inputString)) {
                    output.setText(EncodeNormalMorseManager.getEncodedString(inputString));
                } else {
                    output.setText(DecodeNormalMorseManager.getDecodedString(inputString));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public void copyNormalMorse(FragmentActivity fragmentActivity) {
        TextView message = (TextView) fragmentActivity.findViewById(R.id.outputNormalMorse);
        MainActivity.copy(message.getText().toString(), fragmentActivity);
    }

    public void shareNormalMorse(FragmentActivity fragmentActivity) {
        TextView message = (TextView) fragmentActivity.findViewById(R.id.outputNormalMorse);
        MainActivity.share(message.getText().toString(), fragmentActivity);
    }
}