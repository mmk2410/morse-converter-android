package de.marcelkapfer.morseconverter.fragments;

/*
    This is a Android application for converting writtenMorse and normal morse code.
    Copyright (C) 2014-2015  Marcel Michael Kapfer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.
    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    Marcel Michael Kapfer
    marcelmichaelkapfer@yahoo.co.nz

 */

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import de.marcelkapfer.morseconverter.R;

/**
 * Created by mmk on 2/14/15.
 */
public class AboutFragment extends Fragment{
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onStart(){

        LinearLayout versionEntryButton = (LinearLayout) getActivity().findViewById(R.id.versionEntry);

        versionEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                versionEntry();
            }
        });

        LinearLayout developerEntryButton = (LinearLayout) getActivity().findViewById(R.id.developerEntry);

        developerEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                developerEntry();
            }
        });

        LinearLayout websiteEntryButton = (LinearLayout) getActivity().findViewById(R.id.websiteEntry);

        websiteEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                websiteEntry();
            }
        });

        LinearLayout contactEntryButton = (LinearLayout) getActivity().findViewById(R.id.contactEntry);

        contactEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                contactEntry();
            }
        });

        LinearLayout licenseEntryButton = (LinearLayout) getActivity().findViewById(R.id.licenseEntry);

        licenseEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                licenseEntry();
            }
        });

        LinearLayout bugEntryButton = (LinearLayout) getActivity().findViewById(R.id.bugEntry);

        bugEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bugEntry();
            }
        });

        LinearLayout howtoEntryButton = (LinearLayout) getActivity().findViewById(R.id.howtoEntry);

        howtoEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                howtoEntry();
            }
        });

        LinearLayout missingCodeEntryButton = (LinearLayout) getActivity().findViewById(R.id.missingCodeEntry);

        missingCodeEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                missingCodeEntry();
            }
        });

        super.onStart();
    }

    //called when clicking on the version entry in the about fragment
    //opens the GitLab repository
    public void versionEntry(){
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://gitlab.com/mmk2410/morse-converter-android"));
            startActivity(browserIntent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    //called when clicking on the developer entry in the about fragment
    //opens the website of Marcel Kapfer
    public void developerEntry(){
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://mmk2410.org"));
            startActivity(browserIntent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    //called when clicking on the website entry in the about fragment
    //opens the writtenMorse wiki page at phab.mmk2410.org
    public void websiteEntry(){
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://phab.mmk2410.org/w/writtenmorse/"));
            startActivity(browserIntent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    //called when clicking on the contact entry in the about fragment
    //opens mail app with mail to opensource@mmk2410.org
    public void contactEntry(){
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:opensource@mmk2410.org"));
            startActivity(browserIntent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    //called when clicking on the license entry in the about fragment
    //opens the license web page
    public void licenseEntry(){
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://gnu.org/copyleft/gpl.html"));
            startActivity(browserIntent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    //called when clicking on the developer entry in the about fragment
    //opens mail app with mail to opensource@mmk2410.org
    public void bugEntry(){
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:opensource@mmk2410.org?subject=writtenMorse Bug Report"));
            startActivity(browserIntent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    //called when clicking on the developer entry in the about fragment
    //opens the writtenMorse wiki page on phab.mmk2410.org
    public void howtoEntry(){
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://phab.mmk2410.org/w/writtenmorse/"));
            startActivity(browserIntent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    //called when clicking on the developer entry in the about fragment
    //opens mail app with mail to marcelmichaelkapfer@yahoo.co.nz
    public void missingCodeEntry(){
        try{
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("mailto:opensource@mmk2410.org?subject=writtenMorse Missing Code"));
            startActivity(browserIntent);
        } catch(Exception e){
            e.printStackTrace();
        }
    }

}
