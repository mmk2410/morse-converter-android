# writtenMorse Android Converter

A writtenMorse and normal morse code converter for Android. You can find more information about this app on the [GitLab Wiki](https://gitlab.com/mmk2410/writtenmorse-specs/wikis/Home).

![Image](https://gitlab.com/mmk2410/morse-converter-android/raw/master/feature-graphic.png)

Get it on [Google Play](https://play.google.com/store/apps/details?id=de.marcelkapfer.morseconverter)

## Contribute

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Add yourself to the CONTRIBUTORS file
5. Push to the branch (`git push origin my-new-feature`)
6. **Create a new merge request**

You can always write me a mail at [me@mmk2410.org](mailto:me@mmk2410.org) or contact me on [Twitter](https://twitter.com/mmk2410).

## Beta testing

Would you like to test new features before they land in the public version? Just join the [Google+ community](https://plus.google.com/communities/103668718628585126049).

## Used Libraries

* [Neokree's Material Navigation Drawer](https://github.com/neokree/MaterialNavigationDrawer)
